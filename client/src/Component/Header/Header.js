import React from 'react'
import {Navbar,Nav,Container} from 'react-bootstrap'
// import {Link} from 'react-router-dom'
export default function Header() {
  return (
    <div>
      <Navbar collapseOnSelect expand="lg" bg="warning" variant="success" style={{height:100}}>
  <Container>
  <Navbar.Brand href="/">
    <strong>ADMIN DASHBOARD</strong>
  </Navbar.Brand>
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="me-auto">
      
      
    </Nav>
    <Nav style={{fontSize:30,fontWeight:"bold"}}>
      <Nav.Link href="/login" >Login</Nav.Link>
      <Nav.Link href="/register" >Register</Nav.Link>
     
    </Nav>
  </Navbar.Collapse>
  </Container>
</Navbar>
      
    </div>
  )
}
