import React, { Component } from "react";
const DefaultForm = {
  FirstName: "",
  MiddleName: "",
  LastName: "",
  email: "",
  username: "",
  password: "",
  permanent_address: "",
  temp_address: "",
  dob: "",
  conatc: "",
  role: "",
};
export default class Register extends Component {
  constructor() {
    super();
    this.state = {
      data: { ...DefaultForm },
      error: { ...DefaultForm },
    };
  }
  handleChange = (e) => {
    const { name, value } = e.target;
    this.setState(
      (prev) => ({
        data: {
          ...prev.data,
          [name]: value,
        },
      }),
      () => {
        this.ValidateForm(name);
      }
    );
  };

  ValidateForm(FieldName) {
    let Errmsg;
    switch (FieldName) {
      case "username":
        Errmsg = this.state.data[FieldName] ? "" : "Username is Require";
        break;
      case "password":
        Errmsg = this.state.data[FieldName]
          ? this.state.data[FieldName].length > 5
            ? ""
            : "Weak Password"
          : "Password is required";
        break;
      case "email":
        Errmsg = this.state.data[FieldName]
          ? this.state.data[FieldName].includes("@") &&
            this.state.data[FieldName].includes(".com")
            ? ""
            : "Email is Invalid"
          : "Email is required";
        break;

      default:
        break;
    }
    this.setState((prev)=>({
     error:{
       ...prev.error,
      [FieldName]:Errmsg
     }
    }),()=>{

    })
  }
  handleSubmit = (e) => {
    e.PreventDefault();
  };

  render() {
    return (
      <div className="container">
        <div
          className="row"
          style={{ justifyContent: "center", marginTop: 40 }}
        >
          <div className="col-4">
            <form>
              <div class="mb-3">
                <input
                  placeholder="ENter Your FirstName"
                  type="text"
                  name="FirstName"
                  className="form-control"
                  onChange={this.handleChange}
                />
              </div>
            </form>
          </div>
          <div className="col-4">
            <form>
              <div class="mb-3">
                <input
                  placeholder="ENter Your Middle Name"
                  type="text"
                  name="MiddleName"
                  class="form-control"
                  onChange={this.handleChange}
                />
              </div>
            </form>
          </div>
          <div className="col-4">
            <form>
              <div class="mb-3">
                <input
                  placeholder="ENter Your Last Name"
                  type="text"
                  name="LastName"
                  class="form-control"
                  onChange={this.handleChange}
                />
              </div>
            </form>
          </div>
        </div>
        {/* // */}
        <div
          className="row"
          style={{ justifyContent: "center", marginTop: 40 }}
        >
          <div className="col-4">
            <form>
              <div class="mb-3">
                <input
                  placeholder="Enter an Email"
                  type="email"
                  name="email"
                  class="form-control"
                  onChange={this.handleChange}
                />
                <p>{this.state.error.email}</p>
              </div>
            </form>
          </div>
          <div className="col-4">
            <form>
              <div class="mb-3">
                <input
                  placeholder="Enter UserName"
                  type="text"
                  name="username"
                  class="form-control"
                  onChange={this.handleChange}
                />
                <p>{this.state.error.username}</p>
              </div>
            </form>
          </div>
          <div className="col-4">
            <form>
              <div class="mb-3">
                <input
                  placeholder="ENter Your Password"
                  type="password"
                  name="password"
                  class="form-control"
                  onChange={this.handleChange}
                  />
                  <p>{this.state.error.password}</p>
              </div>
            </form>
          </div>
        </div>
        {/*  */}

        <div
          className="row"
          style={{ justifyContent: "center", marginTop: 40 }}
        >
          <div className="col-4">
            <form>
              <div class="mb-3">
                <input
                  placeholder="Enter Your Date Of BIrth"
                  type="Date"
                  name="dob"
                  className="form-control"
                  onChange={this.handleChange}
                />
              </div>
            </form>
          </div>
          <div className="col-4">
            <form>
              <div class="mb-3">
                <input
                  placeholder="Enter Your Permanent Address"
                  type="text"
                  name="permanent_address"
                  class="form-control"
                  onChange={this.handleChange}
                />
              </div>
            </form>
          </div>
          <div className="col-4">
            <form>
              <div class="mb-3">
                <input
                  placeholder="ENter Your Temp_Address"
                  type="text"
                  name="temp_address"
                  className="form-control"
                  onChange={this.handleChange}
                  aria-describedby="temp_address"
                />
              </div>
            </form>
          </div>
        </div>
        {/*  */}

        <div
          className="row"
          style={{ justifyContent: "center", marginTop: 40 }}
        >
          <div className="col-1">
            <div class="form-check">
              <input
                class="form-check-input"
                type="radio"
                name="gender"
                id="gender"
                onChange={this.handleChange}
                checked
              />
              <label className="form-check-label" for="gender">
                MALE
              </label>
            </div>
          </div>
          <div className="col-1">
            <div class="form-check">
              <input
                class="form-check-input"
                type="radio"
                name="gender"
                id="female"
                onChange={this.handleChange}
                checked
              />
              <label class="form-check-label" for="gender">
                FEMALE
              </label>
            </div>
          </div>
          <div className="col-2">
            <div class="form-check">
              <input
                class="form-check-input"
                type="radio"
                name="other"
                onChange={this.handleChange}
                id="gender"
                checked
              />
              <label class="form-check-label" for="gender">
                OTHER
              </label>
            </div>
          </div>
          <div className="col-4">
            <form>
              <div class="mb-3">
                <input
                  placeholder="ENter Your Contact Number"
                  type="Number"
                  name="contact"
                  onChange={this.handleChange}
                  className="form-control"
                />
              </div>
            </form>
          </div>
          <div className="col-4">
            <form>
              <div className="mb-3">
                <input
                  placeholder="ENter Your role"
                  type="text"
                  className="form-control"
                  onChange={this.handleChange}
                />
              </div>
            </form>
          </div>
          <div className="row">
            <div className="col">
              <button
                type="button"
                onSubmit={this.handleSubmit}
                className="btn btn-outline-success"
                style={{
                  justifyContent: "left",
                  height: "50px",
                  width: "160px",
                }}
              >
                <strong>REGISTER HERE</strong>
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
