import React, { Component } from "react";
import './style.css';
export default class Home extends Component {
  render() {
    return (
        <div className="jumbotron jumbotron-fluid margin">
        <div className="container-fluid">
          <h1 className="display-4">WELCOME HERE</h1>
          <p className="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
        </div>
      </div>
    );
  }
}
