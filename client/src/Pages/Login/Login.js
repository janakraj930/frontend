import React, { Component } from "react";
// import { Form, Button, Container, Row, Col } from "react-bootstrap";
export default class Login extends Component {
 constructor(){
   super()
   this.state={
     data:{
       username:'',
       password:''
     },
     error:{
       username:'',
       password:''
     }
   }
 }
handleChange=(e)=>{
  const {name,value}=e.target
  this.setState((prev)=>({
    data:{
      ...prev.data,
      [name]:value
    },
   

  }),()=>{
    this.ValidateForm(name)

  })
}
 
ValidateForm(FieldName){
  let Err;
  switch(FieldName){
    case 'username':
      Err=this.state.data[FieldName]
      ? ''
      :'UserName is Required'
      break;
      case 'password':
        Err=this.state.data[FieldName]
        ? this.state.data[FieldName].length>4
        ? ''
        :'Weak Password'
        :'Password is required'
        break;
        default:
          break;
  }
  this.setState((prev)=>({
    error:{
      ...prev.data,
      [FieldName]:Err
    }
  }))
}
  render() {
    return (
      <div >
        <div className="container-fluid">
          <div className="row" style={{justifyContent:"center",marginTop:"60px"}}>
            <div className="col-4">
              <form>
                <div class="mb-3">
                  <input
                  placeholder="ENter Your UserName"
                    type="text"
                    name="username"
                    className="form-control"
                    onChange={this.state.handleChange}
                  />
                  <p>{this.state.error.username}</p>
                 
                </div>
                <div class="mb-3">
                 
                  <input
                    type="password"
                    name="password"
                    className="form-control"
                    id="password"
                    onChange={this.state.handleChange}
                    placeholder="***************************"
                  />
                  <p>{this.state.error.password}</p>
                </div>
              
                <button type="submit" onSubmit={this.state.handleSubmit} className="btn btn-primary">
                  <strong>LOGIN</strong>
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
